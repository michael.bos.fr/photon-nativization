// Copyright Epic Games, Inc. All Rights Reserved.

#include "PhotonNativization.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PhotonNativization, "PhotonNativization" );
