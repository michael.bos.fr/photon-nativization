// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PhotonNativizationGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PHOTONNATIVIZATION_API APhotonNativizationGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
